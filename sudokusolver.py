
import numpy as np

matrix = [[0,6,2,8,0,3,1,9,0],
        [4,0,0,1,0,6,0,0,8],
        [0,9,1,0,4,0,0,0,0],
        [0,0,0,0,8,0,0,1,7],
        [0,0,9,3,1,4,8,0,0],
        [1,0,0,0,0,7,0,0,2],
        [0,0,6,0,0,0,3,2,0],
        [2,0,0,6,0,0,0,0,9],
        [0,1,4,7,0,9,5,8,0]]

def isSolutionNum(row, column, num):
    global matrix
    
    for i in range(0,9):
        if matrix[row][i] == num:
            return False

    for i in range(0,9):
        if matrix[i][column] == num:
            return False
    
    x0 = (column // 3) * 3
    y0 = (row // 3) * 3
    for i in range(0,3):
        for j in range(0,3):
            if matrix[y0+i][x0+j] == num:
                return False

    return True

def solution():
    global matrix
    for row in range(0,9):
        for column in range(0,9):
            if matrix[row][column] == 0:
                for num in range(1,10):
                    if isSolutionNum(row, column, num):
                        matrix[row][column] = num
                        solution()
                        matrix[row][column] = 0

                return
      
    print(np.matrix(matrix))
    input('More than one solution exists')

solution()


